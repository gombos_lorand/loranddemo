## Installation
* Copy the code *www/html
* Install packages with composer using the following command:
```bash
$ composer install
```
* Rename (`.env.example`) to (`.env`)
* Setup the DB credentials
* Create DB structure using the following command:
```bash
$ php artisan migrate
```
* Install demo DB data using the following command
```bash
$ php artisan db:seed
```