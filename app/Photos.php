<?php
/**
 * This file is part of the LorandDemo project.
 *
 * (c) Gombos Lorand  https://about.me/gombos.lorand
 *
 * Created by lorand.gombos at 02/09/2016.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App;

use App\Observers\PhotoObserver;
use Jenssegers\Mongodb\Model;

/**
 * Class Photos
 * @package App
 *
 * @property integer $id
 * @property string $name
 * @property string $extension
 * @property string $entity_id
 * @property string $entity_type
 * @property integer $main
 * @property string $fullName
 * @property string $path
 */
class Photos extends Model
{
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'extension'
    ];

    public function entity()
    {
        return $this->morphTo();
    }

    public static function boot(){
        parent::boot();
        self::observe(new PhotoObserver());
    }

    public function getFullNameAttribute()
    {
        return $this->attributes['name'] .'.'. $this->attributes['extension'];
    }

    public function getPathAttribute(){
        $path = \Config::get('settings.UPLOAD_PATH');
        if(User::class == $this->attributes['entity_type']){
            $path = \Config::get('settings.USER_IMG_PATH');
        }
        $path .= $this->attributes['name'] .'.'. $this->attributes['extension'];
        return $path;
    }
}
