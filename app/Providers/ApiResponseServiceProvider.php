<?php

namespace App\Providers;

use Request;
use App\Services\ApiResponse;
use Illuminate\Support\ServiceProvider;
use League\Fractal\Manager;

class ApiResponseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        $manager = new Manager;

        // If you have to customize the manager instance, like setting a custom serializer,
        // I strongly suggest you to create your own service provider and add you manager configuration action here
        // Here some example if you want to set a custom serializer :
        // $manager->setSerializer(\League\Fractal\Serializer\JsonApiSerializer);

        // Are we going to try and include embedded data?
        $includes = explode(',', Request::input('include'));
        $includes = array_map('trim',$includes);
        $manager->parseIncludes($includes);

        // Return the Response object
        $response = new ApiResponse($manager);

        //Set the response instance properly
        $this->app->instance('EllipseSynergie\ApiResponse\Contracts\Response', $response);

        return $response;
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
