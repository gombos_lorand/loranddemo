<?php
/**
 * This file is part of the LorandDemo project.
 *
 * (c) Gombos Lorand  https://about.me/gombos.lorand
 *
 * Created by lorand.gombos at 02/09/2016.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Traits;

trait MongoEmbedTranslatableTrait
{

    private $arrayData;

    /**
     * @param $key
     * @return mixed
     */
    public function getAttribute($key)
    {
        $default_locale = true;
        if (str_contains($key, ':')) {
            $segments = explode(':', $key);
            if (count($segments) == 2) {
                list($key, $locale) = $segments;
            } elseif (count($segments) == 3) {
                list($key, $locale, $default_locale) = $segments;
            }
        } else {
            $locale = $this->defaultLocale();
        }
        if ($this->isTranslationAttribute($key)) {
            return $this->translate($key, $locale, (bool)$default_locale);
        }
        return parent::getAttribute($key);
    }

    /**
     * @param $key
     * @param null $locale
     * @param bool $default_locale
     * @return string
     */
    public function translate($key, $locale = null, $default_locale = true)
    {
        $data = $this->getArrayData();
        if (!$this->isAvailableLocale($locale) && $default_locale) {
            $locale = $this->defaultLocale();
        }

        $attribute = array_get($data, $key);
        $string = '';
        if ($this->isTranslationAttribute($key) && is_array($attribute)) {
            $string = array_get($data, $key . '.' . $locale);
            if (empty($string) && $default_locale) {
                $string = array_get($data, $key . '.' . $this->defaultLocale());
            }
        } else {
            if ($locale == $this->defaultLocale() || $default_locale) {
                $string = $attribute;
            }
        }
        return (string)$string;
    }

    /**
     * @param $data
     * @param $key
     * @param $locale
     * @return mixed
     */
    private function _getTranslatedStringFromArray($data, $key, $locale)
    {
        return array_get($data, $key . '.' . $locale);
    }

    /**
     * @param $key
     * @return bool
     */
    protected function isTranslationAttribute($key)
    {
        return in_array($key, $this->translatedAttributes);
    }


    /**
     * @return mixed
     */
    public function defaultLocale()
    {
        return \Config::get('app.fallback_locale');
    }

    /**
     * @param $locale
     * @return bool
     */
    public function isAvailableLocale($locale)
    {
        return in_array($locale, \Config::get('app.locales'));
    }

    public function getArrayData()
    {
        if (!is_array($this->arrayData) || !count($this->arrayData)) {
            $this->arrayData = $this->toArray();
        }
        return $this->arrayData;
    }

}