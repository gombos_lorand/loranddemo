<?php
/**
 * This file is part of the LorandDemo project.
 *
 * (c) Gombos Lorand  https://about.me/gombos.lorand
 *
 * Created by lorand.gombos at 02/09/2016.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App;

use Illuminate\Contracts\Auth\Authenticatable as AuthContract;
use Illuminate\Auth\Authenticatable;
use Jenssegers\Mongodb\Model;

/**
 * Class User
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $name
 * @package App\Models
 */
class User extends Model implements AuthContract
{
    use Authenticatable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function photos()
    {
        return $this->morphOne(Photos::class, 'entity');
    }
}
