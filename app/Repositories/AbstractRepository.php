<?php
/**
 * This file is part of the LorandDemo project.
 *
 * (c) Gombos Lorand  https://about.me/gombos.lorand
 *
 * Created by lorand.gombos at 02/09/2016.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repositories;

use Illuminate\Support\Collection;
use Illuminate\Container\Container as App;
use Bosnadev\Repositories\Eloquent\Repository as ParentRepository;

abstract class AbstractRepository extends ParentRepository
{
    public function __construct()
    {
        $app = new App();
        $collection = new Collection();
        parent::__construct($app, $collection);
    }

    /**
     * @return $this
     * @throws \Bosnadev\Repositories\Exceptions\RepositoryException
     */
    public function resetCriteria()
    {
        $this->criteria = $this->criteria->make();
        $this->makeModel();
        return $this;
    }

    /**
     * @return \Eloquent
     */
    public function getModel()
    {
        return $this->model;
    }
}