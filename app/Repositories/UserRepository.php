<?php
/**
 * This file is part of the LorandDemo project.
 *
 * (c) Gombos Lorand  https://about.me/gombos.lorand
 *
 * Created by lorand.gombos at 02/09/2016.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repositories;

use App\User;

class UserRepository extends AbstractRepository
{

    public function model()
    {
        return User::class;
    }

    public function getByParams($params){
        $data = User::paginate((int)array_get($params,'limit',10));
        return $data;
    }
}
