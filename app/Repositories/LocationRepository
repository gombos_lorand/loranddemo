<?php

namespace App\Repositories;

use App\Models\Location;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class LocationRepository extends Repository {

    public function makeModel()
    {
        $this->model = new Location();
    }

    public function search(array $params){
        $ids = $this->getVisitedLocationIds();

        $result = \DB::table('locations');
        if(array_get($params,'location_name')){
            $result->where('name','LIKE',"%{$params['location_name']}%");
        }
        $result->where('status',Location::STATUS_ACTIVE);
        if(array_get($params,'limit')){
            $data = $result->paginate($params['limit']);
        } else{
            $data = $result->get();
        }
        foreach($data as $location){
            if(in_array($location->id, $ids)){
                $location->visited = true;
            }else{
                $location->visited = false;
            }
        }
        return $data;
    }

    public function searchNearBy(array $params){
        $ids = $this->getVisitedLocationIds();

        if((!array_get($params,'lat') || !array_get($params,'lon')) ||
            ((array_get($params,'map_lat') && !array_get($params,'map_lon')) || (!array_get($params,'map_lat') && array_get($params,'map_lon')))){
            throw new UnprocessableEntityHttpException(trans('errors.invalid_entity',['name'=>'coordinate']));
        }

        $query = '
            SELECT *,
                p.distance_unit
                * DEGREES(ACOS(COS(RADIANS(p.latpoint))
                * COS(RADIANS(z.lat))
                * COS(RADIANS(p.longpoint - z.lon))
                + SIN(RADIANS(p.latpoint))
                * SIN(RADIANS(z.lat)))) AS distance,
                p.distance_unit
                * DEGREES(ACOS(COS(RADIANS(p.maplatpoint))
                * COS(RADIANS(z.lat))
                * COS(RADIANS(p.maplongpoint - z.lon))
                + SIN(RADIANS(p.maplatpoint))
                * SIN(RADIANS(z.lat)))) AS map_distance
            FROM locations AS z
            JOIN (
                SELECT
                  '.\DB::getPdo()->quote(array_get($params,'lat')).' AS latpoint,
                  '.\DB::getPdo()->quote(array_get($params,'lon')).' AS longpoint,
                  '.\DB::getPdo()->quote(array_get($params,'radius',0)).' AS radius,
                  '.\DB::getPdo()->quote(array_get($params,'map_lat')).' AS maplatpoint,
                  '.\DB::getPdo()->quote(array_get($params,'map_lon')).' AS maplongpoint,
                  111.045 AS distance_unit
            ) AS p ON 1=1
        ';
        //if filtered by radius
        if(array_get($params,'radius',0)){
            if((array_get($params,'map_lat') && array_get($params,'map_lon'))){
                $query .= '
            WHERE
            z.lat BETWEEN p.maplatpoint  - (p.radius / p.distance_unit) AND p.maplatpoint  + (p.radius / p.distance_unit)
            AND z.lon BETWEEN p.maplongpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.maplatpoint)))) AND p.maplongpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.maplatpoint))))
            ';
            }else {
                $query .= '
            WHERE
            z.lat BETWEEN p.latpoint  - (p.radius / p.distance_unit) AND p.latpoint  + (p.radius / p.distance_unit)
            AND z.lon BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint)))) AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
            ';
            }
        }

        $result = \DB::table(\DB::raw(' ('.$query.') as _main'));
        if(array_get($params,'location_name')){
            $result->where('name','LIKE',"%{$params['location_name']}%");
        }
        if(array_get($params,'radius',0)){
            if((array_get($params,'map_lat') && array_get($params,'map_lon'))){
                $result->where('map_distance', '<=', \DB::raw('radius'));
            }else {
                $result->where('distance', '<=', \DB::raw('radius'));
            }
        }
        $result->where('status',Location::STATUS_ACTIVE);
        $result->orderBy(array_get($params,'sort','distance'),array_get($params,'sort_order','asc'));
        if(array_get($params,'limit')){
            $data = $result->paginate($params['limit']);
        } else{
            $data = $result->get();
        }
        foreach($data as $location){
            if(in_array($location->id, $ids)){
                $location->visited = true;
            }else{
                $location->visited = false;
            }
        }

        return $data;
    }
}