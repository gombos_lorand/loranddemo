<?php
/**
 * This file is part of the LorandDemo project.
 *
 * (c) Gombos Lorand  https://about.me/gombos.lorand
 *
 * Created by lorand.gombos at 02/09/2016.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Transformer;


use App\Models\Photo;
use App\Photos;
use League\Fractal\TransformerAbstract;

class PhotoTransformer extends TransformerAbstract {

    public function transform(Photos $photo){
        $array = [
            'id' => $photo->id,
            'name' => $photo->name,
            'extension' => $photo->extension,
            'full_name' => $photo->fullName,
            'path' => $photo->path,
            'url' => url($photo->path),
        ];
        return $array;
    }
}