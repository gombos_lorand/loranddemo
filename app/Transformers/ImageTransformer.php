<?php
/**
 * This file is part of the LorandDemo project.
 *
 * (c) Gombos Lorand  https://about.me/gombos.lorand
 *
 * Created by lorand.gombos at 02/09/2016.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Transformers;

use Faker\Factory;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

class ImageTransformer extends TransformerAbstract
{
    public function transform()
    {
        $faker = new Factory();
        $faker = $faker->create();
        $array = [
            'id' => $faker->numberBetween(1,1000),
            'image' => $faker->imageUrl('200','300')
        ];
        return $array;
    }
}