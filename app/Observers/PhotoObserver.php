<?php

namespace App\Observers;

use App\Photos;

class PhotoObserver
{
    public function deleting(Photos $photo)
    {
        \File::delete(public_path($photo->path));
        return true;
    }
}
