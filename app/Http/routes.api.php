<?php
/**
 * This file is part of the LorandDemo project.
 *
 * (c) Gombos Lorand  https://about.me/gombos.lorand
 *
 * Created by lorand.gombos at 02/09/2016.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

Route::post('auth', 'AuthController@index');

Route::group(['middleware'=> 'jwt.auth'], function(){
    Route::get('auth/user', 'AuthController@get_user');
    Route::resource('self', 'UserSelfController', ['only' => ['index', 'store']]);
    Route::resource('users', 'UserController', ['only' => ['index']]);
});