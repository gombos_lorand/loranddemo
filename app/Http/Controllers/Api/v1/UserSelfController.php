<?php
/**
 * This file is part of the LorandDemo project.
 *
 * (c) Gombos Lorand  https://about.me/gombos.lorand
 *
 * Created by lorand.gombos at 02/09/2016.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\ApiController;
use App\Http\Requests\api\UserProfileUpdate;
use App\Photos;
use App\Repositories\UserRepository;
use App\Services\PhotoService;
use App\Transformers\UserTransformer;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class UserSelfController extends ApiController
{
    /**
     * @SWG\Get(path="/self",
     *      tags={"Self"},
     *      summary="Get authenticated user",
     *      description="",
     *      operationId="authUser",
     *      @SWG\Response(response="default", description="successful operation")
     *  )
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     */
    public function index()
    {
        $user = \JWTAuth::parseToken()->authenticate();
        return $this->response->withItem($user, new UserTransformer());
    }

    /**
     * @SWG\Post(path="/self",
     *      tags={"Self"},
     *      summary="Update logged in user profile",
     *      description="",
     *      operationId="updateUserData",
     *      @SWG\Parameter(
     *          in="formData",
     *          name="email",
     *          description="User e-mail",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Parameter(
     *          in="formData",
     *          name="name",
     *          description="User name",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Parameter(
     *          in="formData",
     *          name="birthday",
     *          description="User birthday",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Parameter(
     *          in="formData",
     *          name="gender",
     *          description="User gender",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Parameter(
     *          name="file",
     *          in="formData",
     *          description="Image",
     *          required=false,
     *          type="file"
     *      ),
     *      @SWG\Response(response="default", description="successful operation")
     *  )
     * @param UserProfileUpdate $request
     * @return mixed
     */
    public function store(UserProfileUpdate $request)
    {
        $userData = array_only($request->all(), ['email', 'name', 'birthday', 'gender']);
        $userRepository = new UserRepository();
        if (!$userRepository->update($userData, \Auth::id(), '_id')) {
            throw new UnprocessableEntityHttpException(trans('errors.update_entity', ['entity' => 'User']));
        }

        //upload profile image if exists
        if ($request->file('file')) {
            $PhotoService = new PhotoService();
            $photoData = $PhotoService->upload($request->file('file'));
            $photo = new Photos([
                'name' => array_get($photoData, 'name'),
                'extension' => array_get($photoData, 'extension'),
                'main' => false
            ]);
            $user = \Auth::user();
            if (!$user->photos()->save($photo)) {
                throw new UnprocessableEntityHttpException(trans('errors.update_entity', ['entity' => 'user photo']));
            }
        }
        $user = $userRepository->find(\Auth::id());
        return $this->response->withItem($user, new UserTransformer());
    }
}
