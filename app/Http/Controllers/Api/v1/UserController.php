<?php
/**
 * This file is part of the LorandDemo project.
 *
 * (c) Gombos Lorand  https://about.me/gombos.lorand
 *
 * Created by lorand.gombos at 02/09/2016.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\ApiController;
use App\Repositories\UserRepository;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use App\Http\Requests;

class UserController extends ApiController
{
    /**
     * @SWG\Get(path="/users",
     *      tags={"Users"},
     *      summary="Get users by params",
     *      description="",
     *      operationId="getUsers",
     *      @SWG\Parameter(
     *          in="query",
     *          name="limit",
     *          description="Limit",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Parameter(
     *          in="query",
     *          name="page",
     *          description="Page",
     *          required=false,
     *          type="string"
     *      ),
     *      @SWG\Response(response="default", description="successful operation")
     *  )
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $userRepository = new UserRepository();
        $result = $userRepository->getByParams($params);
        return $this->response->withPaginator($result, new UserTransformer());
    }
}
