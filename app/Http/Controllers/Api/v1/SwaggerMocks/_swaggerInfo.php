<?php
/**
 * This file is part of the LorandDemo project.
 *
 * (c) Gombos Lorand  https://about.me/gombos.lorand
 *
 * Created by lorand.gombos at 02/09/2016.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * @SWG\Swagger(
 *     schemes={"http"},
 *     produces={"application/json"},
 *     host="",
 *     basePath="/api/v1",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="API Specification",
 *         description="API Specification",
 *         @SWG\Contact(
 *             email="gombos.lorand@gmail.com"
 *         )
 *     )
 * )
 *
 * @SWG\SecurityScheme(
 *   securityDefinition="api_key",
 *   type="apiKey",
 *   in="query",
 *   name="api_key"
 * )
 *
 * @SWG\Tag(
 *   name="External",
 *   description="External API - Operations for the ERP"
 * )
 * @SWG\Tag(
 *   name="Carousels",
 *   description="Operation about carousels"
 * )
 * @SWG\Tag(
 *   name="MediaLibrary",
 *   description="Operation about media library"
 * )
 * @SWG\Tag(
 *   name="Products",
 *   description="Operations about products"
 * ),
 * @SWG\Tag(
 *   name="Tags",
 *   description="Operations about tags"
 * ),
 * @SWG\Tag(
 *   name="Cart",
 *   description="Operations about cart"
 * ),
 * @SWG\Tag(
 *   name="Orders",
 *   description="Operations about orders"
 * )
 */
