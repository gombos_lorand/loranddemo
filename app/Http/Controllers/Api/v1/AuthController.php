<?php
/**
 * This file is part of the LorandDemo project.
 *
 * (c) Gombos Lorand  https://about.me/gombos.lorand
 *
 * Created by lorand.gombos at 02/09/2016.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\ApiController;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends ApiController
{
    /**
     * @SWG\Post(path="/auth",
     *      tags={"Auth"},
     *      summary="User Auth",
     *      description="",
     *      operationId="auth",
     *      @SWG\Parameter(
     *          in="formData",
     *          name="email",
     *          description="User e-mail",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Parameter(
     *          in="formData",
     *          name="password",
     *          description="User password",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Response(response="default", description="successful operation")
     *  )
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     */
    public function index(Request $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            if (!$token = \JWTAuth::attempt($credentials)) {
                return $this->response->setStatusCode(401)->withError('invalid_credentials', 401);
            }
        } catch (JWTException $e) {
            return $this->response->setStatusCode(500)->withError('could_not_create_token', 500);
        }
        return response()->json(compact('token'));
    }

    /**
     * @SWG\Get(path="/auth/user",
     *      tags={"Auth"},
     *      summary="Get authenticated user",
     *      description="",
     *      operationId="authUser",
     *      @SWG\Response(response="default", description="successful operation")
     *  )
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     */
    public function get_user()
    {
        $user = \JWTAuth::parseToken()->authenticate();
        return $this->response->withItem($user, new UserTransformer());
    }
}
