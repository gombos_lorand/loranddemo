<?php
/**
 * This file is part of the LorandDemo project.
 *
 * (c) Gombos Lorand  https://about.me/gombos.lorand
 *
 * Created by lorand.gombos at 02/09/2016.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace App\Http\Controllers;

use App\Services\ApiResponse;
use EllipseSynergie\ApiResponse\Contracts\Response;

class ApiController extends Controller
{
    /**
     * @var ApiResponse
     */
    public $response;

    /**
     * @param Response $response
     */
    public function __construct(Response $response)
    {
        $this->response = $response;
    }
}