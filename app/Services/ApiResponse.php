<?php
/**
 * This file is part of the LorandDemo project.
 *
 * (c) Gombos Lorand  https://about.me/gombos.lorand
 *
 * Created by lorand.gombos at 02/09/2016.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Services;

use EllipseSynergie\ApiResponse\Laravel\Response;

class ApiResponse extends Response
{

    public function withError($message, $errorCode, $exception = '')
    {
        $response = [
            'error' => [
                'code' => $errorCode,
                'http_code' => $this->statusCode,
                'message' => $message
            ]
        ];

        if (\Config::get('settings.app_detailed_exception', false)) {
            $response['error']['exception'] = @(string)$exception;
        }

        return $this->withArray($response);
    }
}
