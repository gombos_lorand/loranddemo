<?php
/**
 * This file is part of the LorandDemo project.
 *
 * (c) Gombos Lorand  https://about.me/gombos.lorand
 *
 * Created by lorand.gombos at 02/09/2016.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Services;


use App\User;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PhotoService
{
    /**
     * @param string $scope
     * @return string
     */
    public function getUploadDir($scope)
    {
        $path = \Config::get('settings.UPLOAD_PATH');
        if(User::class == $scope){
            $path = \Config::get('settings.USER_IMG_PATH');
        }
        return $path;
    }

    /**
     * @return string
     */
    public static function generateName()
    {
        return md5(uniqid(rand(), true));
    }

    /**
     * @param UploadedFile $file
     * @param $scope
     * @return array
     */
    public function upload(UploadedFile $file, $scope = User::class)
    {
        $extension = $file->getClientOriginalExtension();
        $name = $this->generateName();
        $uploadDir = $this->getUploadDir($scope);
        $file->move(
            $uploadDir,
            $name . '.' . $extension
        );
        return [
            'extension' => $extension,
            'name' => $name,
            'original_name' => $file->getClientOriginalName(),
            'upload_dir' => $uploadDir
        ];
    }
}