<?php
namespace App\Services\SocialProviders;


use App\Services\SocialProviders\Entity\SocialUserContract;
use League\OAuth2\Client\Token\AccessToken;

class GoogleUser implements SocialUserContract
{

    private $access_token;

    public function __construct($access_token)
    {
        $this->access_token = $access_token;
    }

    public function getUser()
    {
        $google = new Google();
        $userDetails = $google->getUserDetails(new AccessToken(['access_token' => $this->access_token]));
        $response = [
            'email' => $userDetails->email,
            'first_name' => $userDetails->firstName,
            'last_name' => $userDetails->lastName,
            'google_access_token' => $this->access_token,
            'birthday' => date('Y-m-d', strtotime($userDetails->birthday)),
            'picture_url' => $userDetails->imageUrl,
            'gender' => $userDetails->gender
        ];
        return $response;
    }
}