<?php

namespace App\Services\SocialProviders;


use App\Services\SocialProviders\Entity\SocialUserContract;
use League\OAuth1\Client\Credentials\TokenCredentials;

class TwitterUser implements SocialUserContract
{
    private $credentials;

    public function __construct($credentials)
    {
        $this->credentials = $credentials;
    }

    public function getUser()
    {
        $twitter = new Twitter(array(
            'identifier' => \Config::get('social_provider.consumers.Twitter.identifier'),
            'secret' => \Config::get('social_provider.consumers.Twitter.secret')
        ));

        $token = new TokenCredentials();
        $token->setIdentifier(array_get($this->credentials, 'identifier'));
        $token->setSecret(array_get($this->credentials, 'secret'));
        $userDetails = $twitter->getUserDetails($token);
        $nameArray = explode(' ', $userDetails->name, 2);
        $response = [
            'email' => $userDetails->email,
            'first_name' => array_get($nameArray, 0),
            'last_name' => array_get($nameArray, 1),
            'twitter_access_token' => $userDetails->uid,
            'picture_url' => $userDetails->imageUrl
        ];
        return $response;
    }
}