<?php

namespace App\Services\SocialProviders;


use App\Services\SocialProviders\Entity\User;

class Facebook extends \League\OAuth2\Client\Provider\Facebook {
    /**
     * @param \League\OAuth2\Client\Token\AccessToken $token
     * @return string
     */
    public function urlUserDetails(\League\OAuth2\Client\Token\AccessToken $token)
    {
        $fields = implode(',', [
            'id',
            'name',
            'first_name',
            'last_name',
            'email',
            'hometown',
            'bio',
            'picture.type(large){url}',
            'gender',
            'locale',
            'link',
            'birthday'
        ]);

        return 'https://graph.facebook.com/'.$this->graphApiVersion.'/me?fields='.$fields.'&access_token='.$token;
    }

    /**
     * @param $response
     * @param \League\OAuth2\Client\Token\AccessToken $token
     * @return User
     */
    public function userDetails($response, \League\OAuth2\Client\Token\AccessToken $token)
    {
        $user = new User();

        $email = (isset($response->email)) ? $response->email : null;
        // The "hometown" field will only be returned if you ask for the `user_hometown` permission.
        $location = (isset($response->hometown->name)) ? $response->hometown->name : null;
        $description = (isset($response->bio)) ? $response->bio : null;
        $imageUrl = (isset($response->picture->data->url)) ? $response->picture->data->url : null;
        $gender = (isset($response->gender)) ? $response->gender : null;
        $locale = (isset($response->locale)) ? $response->locale : null;
        $birthday = (isset($response->birthday)) ? $response->birthday : null;
        $link = isset($response->link)? $response->link : null;

        $user->exchangeArray([
            'uid' => $response->id,
            'name' => $response->name,
            'firstname' => $response->first_name,
            'lastname' => $response->last_name,
            'email' => $email,
            'location' => $location,
            'description' => $description,
            'imageurl' => $imageUrl,
            'gender' => $gender,
            'locale' => $locale,
            'urls' => [ 'Facebook' => $link ],
            'birthday' => $birthday
        ]);

        return $user;
    }
}