<?php
namespace App\Services\SocialProviders;


use App\Services\SocialProviders\Entity\SocialUserContract;
use League\OAuth2\Client\Token\AccessToken;

class FacebookUser implements SocialUserContract
{

    private $access_token;

    public function __construct($access_token)
    {
        $this->access_token = $access_token;
    }

    public function getUser()
    {
        $fb = new Facebook([]);
        $userDetails = $fb->getUserDetails(new AccessToken(['access_token' => $this->access_token]));
        $response = [
            'email' => $userDetails->email,
            'first_name' => $userDetails->firstName,
            'last_name' => $userDetails->lastName,
            'facebook_access_token' => $this->access_token,
            'birthday' => date('Y-m-d', strtotime($userDetails->birthday)),
            'picture_url' => $userDetails->imageUrl,
            'gender' => $userDetails->gender
        ];
        return $response;
    }
}