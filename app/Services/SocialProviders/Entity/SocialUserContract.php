<?php
/**
 * Created by PhpStorm.
 * User: lorand.gombos
 * Date: 2/9/2016
 * Time: 12:42 PM
 */

namespace App\Services\SocialProviders\Entity;


interface SocialUserContract
{
    public function getUser();
}