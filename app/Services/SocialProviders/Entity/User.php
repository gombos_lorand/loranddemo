<?php

namespace App\Services\SocialProviders\Entity;


class User extends \League\OAuth2\Client\Entity\User
{

    protected $birthday;

    /**
     * @return array
     */
    public function getArrayCopy()
    {
        return [
            'uid' => $this->uid,
            'nickname' => $this->nickname,
            'name' => $this->name,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'email' => $this->email,
            'location' => $this->location,
            'description' => $this->description,
            'imageUrl' => $this->imageUrl,
            'urls' => $this->urls,
            'gender' => $this->gender,
            'locale' => $this->locale,
            'birthday' => $this->birthday,
        ];
    }

    /**
     * @param array $data
     * @return $this
     */
    public function exchangeArray(array $data)
    {
        foreach ($data as $key => $value) {
            $key = strtolower($key);
            switch ($key) {
                case 'uid':
                    $this->uid = $value;
                    break;
                case 'nickname':
                    $this->nickname = $value;
                    break;
                case 'name':
                    $this->name = $value;
                    break;
                case 'firstname':
                    $this->firstName = $value;
                    break;
                case 'lastname':
                    $this->lastName = $value;
                    break;
                case 'email':
                    $this->email = $value;
                    break;
                case 'location':
                    $this->location = $value;
                    break;
                case 'description':
                    $this->description = $value;
                    break;
                case 'imageurl':
                    $this->imageUrl = $value;
                    break;
                case 'urls':
                    $this->urls = $value;
                    break;
                case 'gender':
                    $this->gender = $value;
                    break;
                case 'locale':
                    $this->locale = $value;
                    break;
                case 'birthday':
                    $this->birthday = $value;
                    break;
            }
        }

        return $this;
    }
}