<?php

namespace App\Services\SocialProviders;


use App\Services\SocialProviders\Entity\User;

class Google extends \League\OAuth2\Client\Provider\Google
{
    public function urlUserDetails(\League\OAuth2\Client\Token\AccessToken $token)
    {
        return 'https://www.googleapis.com/userinfo/v2/me';
    }

    public function userDetails($response, \League\OAuth2\Client\Token\AccessToken $token)
    {
        $response = (array) $response;
        $user = new User();

        $user->exchangeArray([
            'uid' => array_get($response,'id'),
            'name' => array_get($response,'name'),
            'firstname' => array_get($response,'given_name'),
            'lastName' => array_get($response,'family_name'),
            'email' => array_get($response,'email'),
            'imageUrl' => array_get($response,'picture'),
            'gender' => array_get($response,'gender'),
            'locale' => array_get($response,'locale'),
        ]);
        return $user;
    }
}