<?php
/**
 * Created by PhpStorm.
 * User: lorand.gombos
 * Date: 2/9/2016
 * Time: 11:10 AM
 */
return [
    'UPLOAD_PATH' => env('UPLOAD_PATH','uploads/'),
    'USER_IMG_PATH' => env('USER_IMG_PATH','uploads/users/'),
];